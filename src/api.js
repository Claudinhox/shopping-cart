export const getProducts = async () => {
    try {
        let url = `https://wmslite.hitch.cl/wms/api/v1/products`
        const response = await fetch(url);
        const data = await response.json();
        return data;
    } catch (error) {
    }
};