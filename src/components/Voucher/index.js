import React, { Fragment } from 'react';
import { useCart } from 'react-use-cart';
import accounting from 'accounting';
import Image from '../../images/codebar.svg';

//---------- Material UI ----------//
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

const useStyles = makeStyles((theme) => ({
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    listItem: {
        padding: theme.spacing(1, 0),
    },
    total: {
        fontWeight: 700,
    },
    title: {
        marginBottom: theme.spacing(2),
    },
    subTitle: {
        marginTop: theme.spacing(2),
    },
    image: {
        marginTop: theme.spacing(2),
        width: '100%',
        height: 'auto',
    },
    codeBar: {
        marginBottom: theme.spacing(2),
    },
}));

export default function Voucher() {
    const classes = useStyles();

    const {items, cartTotal} = useCart();

    const num = Math.floor(Math.random()*10000000000000000);
    
    const date = new Date();
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();

    return (
        <Fragment>
            <CssBaseline/>
            <main className={classes.layout}>
                <Paper className={classes.paper}>
                    <Typography className={classes.title} component="h1" variant="h4" align="center">
                        Recibo de Caja
                    </Typography>
                    <Divider variant="middle"/>
                    <ListItem className={classes.listItem}>
                        <ListItemText primary="Nombre del manager:"/>
                        <Typography variant="body2">
                            Usuario
                        </Typography>
                    </ListItem>
                    <ListItem className={classes.listItem}>
                        <ListItemText primary="Fecha:"/>
                        <Typography variant="body2">
                            {day}/{month}/{year}
                        </Typography>
                    </ListItem>
                    <Divider variant="middle"/>
                    <Fragment>
                        <List disablePadding>
                            {items.map((product) => (
                                <ListItem className={classes.listItem} key={product.name}>
                                    <ListItemText primary={product.name} secondary={product.quantity}/>
                                    <Typography variant="body2">
                                        {accounting.formatMoney(product.quantity * product.price, "$")}
                                    </Typography>
                                </ListItem>
                            ))}
                            <Divider variant="middle"/>
                            <ListItem className={classes.listItem}>
                                <ListItemText secondary="Tax"/>
                                  <Typography variant="body2">
                                    {accounting.formatMoney(cartTotal-(cartTotal / 1.19), "$")}
                                </Typography>
                            </ListItem>
                            <ListItem className={classes.listItem}>
                                <ListItemText primary="Total"/>
                                <Typography variant="subtitle1" className={classes.total}>
                                    {accounting.formatMoney(cartTotal, "$")}
                                </Typography>
                            </ListItem>
                            <Divider variant="middle"/>
                            <img className={classes.image} src={Image} alt="CodeBar"/>
                            <Typography className={classes.codeBar} variant="body2" align="center">
                                #{num}#
                            </Typography>
                            <Divider variant="middle"/>
                            <Typography className={classes.subTitle} variant="h6" align="center">
                                Gracias por su compra
                            </Typography>
                        </List>
                    </Fragment>
                </Paper>
            </main>
        </Fragment>
    );
}