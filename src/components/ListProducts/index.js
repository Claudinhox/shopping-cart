import React, { useState } from 'react';
import Product from '../Product';

//---------- Material UI ----------//
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import Pagination from '@material-ui/lab/Pagination';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        padding: theme.spacing(10),
    },
    paper: {
        padding: theme.spacing(1),
        margin: "auto",
        maxWidth: 300,
        backgroundColor: "#4db6ac",
    },
    pagination: {
        marginTop: theme.spacing(2),
        margin: "auto",
        maxWidth: 500,
    }
}));

export default function ListProducts(props) {
    const classes = useStyles();

    const {products, postsPerPage} = props;
    const [search, setSearch] = useState("");

    const handleChange = (event, value) => {
        postsPerPage(value);
    };

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Paper component="form" className={classes.paper}>
                        <InputBase className={classes.input} placeholder="Buscar producto..." onChange={(e) => setSearch(e.target.value)}/>
                        <IconButton className={classes.iconButton} aria-label="search">
                            <SearchIcon/>
                        </IconButton>
                    </Paper>
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                {
                    products.filter((value) => {
                        if (search === "") {
                            return value;
                        } else if (value.name.toLowerCase().includes(search.toLowerCase())) {
                            return value;
                        }
                        return false;
                    }).map((product) => {
                        return (
                            <Grid item xs={12} sm={6} md={4} lg={3}>
                                <Product key={product.id} product={product}/>
                            </Grid>
                        )
                    })
                }
            </Grid>
            <Pagination className={classes.pagination} size='large' variant="outlined" color="primary" count={9} onChange={handleChange}/>
        </div>
    );
}