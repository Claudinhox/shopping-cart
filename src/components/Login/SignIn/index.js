import React, { useState } from 'react';
import { Link as RouteLink, useHistory } from 'react-router-dom';
import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth'

//---------- Material UI ----------//
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: "#4db6ac",
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        backgroundColor: "#4db6ac",
        color: "white",
    },
}));

export default function SignIn() {
    const classes = useStyles();

    const [user, setUser] = useState({
        email: '',
        password: '',
    });

    const history = useHistory();

    const handleChange = (e) => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        firebase.auth().signInWithEmailAndPassword(user.email, user.password)
        .then(() => {
            history.push("/");
            alert('Bienvenido');

        })
        .catch(error => {
            console.log(error);
            alert(error.message)
        });
    };

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Iniciar sesión 
                </Typography>
                <form className={classes.form} onSubmit={handleSubmit}>
                    <TextField variant="outlined" margin="normal" required fullWidth id="email" label="Correo electrónico" name="email" type="email" value={user.email} onChange={handleChange}/>
                    <TextField variant="outlined" margin="normal" required fullWidth id="password" label="Contraseña" name="password" type="password" value={user.password} onChange={handleChange}/>
                    <FormControlLabel control={<Checkbox value="remember" color="primary" />} label="Recuérdame"/>
                    <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
                        Iniciar sesión 
                    </Button>
                    <Grid container>
                        <Grid item xs>
                            <Link href="#" variant="body2">
                                ¿Olvidaste tu contraseña?
                            </Link>
                        </Grid>
                        <Grid item>
                            <RouteLink to="signup">
                                ¿No tienes una cuenta? Registrate
                            </RouteLink>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    );
}