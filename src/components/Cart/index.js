import React from 'react';
import { useCart } from 'react-use-cart';
import accounting from 'accounting';

//---------- Material UI ----------//
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import ClearIcon from '@material-ui/icons/Clear';
import IconButton from '@material-ui/core/IconButton';

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: "#4db6ac",
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 700,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 1700,
    },
    button: {
        backgroundColor: "#4db6ac",
        color: "white",
    }
}));

export default function Cart() {
    const {
        isEmpty,
        items,
        cartTotal,
        updateItemQuantity,
        removeItem,
        emptyCart,
    } = useCart();
  
    const classes = useStyles();
    
    if (isEmpty) return <Paper className={classes.paper}><h1>Su carro esta vacio</h1></Paper>
  
    return (
        <Paper className={classes.paper}>   
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell align="center">Nombre</StyledTableCell>
                            <StyledTableCell align="center">Cantidad</StyledTableCell>
                            <StyledTableCell align="center">Precio Unitario</StyledTableCell>
                            <StyledTableCell align="center">Precio Total</StyledTableCell>
                            <StyledTableCell align="center"></StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {items.map((product) => (
                            <StyledTableRow key={product.id}>
                                <StyledTableCell align="center">{product.name}</StyledTableCell>
                                <StyledTableCell align="center">
                                    <IconButton className={classes.button} size='small' onClick={() => updateItemQuantity(product.id, product.quantity - 1)}>
                                        <RemoveIcon/>
                                    </IconButton >
                                        {product.quantity}
                                    <IconButton className={classes.button} size='small' onClick={() => updateItemQuantity(product.id, product.quantity + 1)}>
                                        <AddIcon/>
                                    </IconButton >
                                </StyledTableCell>
                                <StyledTableCell align="center">{accounting.formatMoney(product.price, "$")}</StyledTableCell>
                                <StyledTableCell align="center">{accounting.formatMoney(product.quantity * product.price, "$")}</StyledTableCell>
                                <StyledTableCell align="center">
                                    <IconButton className={classes.button} size='small' onClick={() => removeItem(product.id)}>
                                        <ClearIcon/>
                                    </IconButton>
                                </StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                    <TableBody>
                        <StyledTableCell align="center"><Button className={classes.button} href="/voucher">Recibo de caja</Button></StyledTableCell>
                        <StyledTableCell align="center"><Button className={classes.button} onClick={() => emptyCart()}>Vaciar Carrito</Button></StyledTableCell>
                        <StyledTableCell align="center">Total:</StyledTableCell>
                        <StyledTableCell align="center">{accounting.formatMoney(cartTotal, "$")}</StyledTableCell>
                        <StyledTableCell align="center"><Button className={classes.button}>Pagar</Button></StyledTableCell>
                    </TableBody>
                </Table>
            </TableContainer>
        </Paper>
    );
}
