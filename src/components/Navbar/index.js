import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useCart } from 'react-use-cart';
import { useUser } from 'reactfire';
import firebase from 'firebase/app';
import 'firebase/auth'

//---------- Material UI ----------//
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import Badge from '@material-ui/core/Badge';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginBottom: "7rem",
    },
    appBar: {
        backgroundColor: "#4db6ac",
        boxShadow: "none",
    },
    grow: {
        flexGrow: 1,
    },
    button: {
        marginLeft: theme.spacing(2),
    },
}));

export default function Navbar() {
    const {totalUniqueItems} = useCart();
    const classes = useStyles();

    const history = useHistory();

    const { data: user } = useUser();

    const handleSignOut = () => {
        firebase.auth().signOut();
        history.push("/");
    };

    return (
        <div className={classes.root}>
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar>
                    <IconButton edge="start" className={classes.menuButton} color="textPrimary" aria-label="menu" href="/">
                        Tienda-Poppy
                    </IconButton>
                    <div className={classes.grow}/>
                    <Typography variant="h6" color="textPrimary" component="p">
                        Hola {user ? user.email : "usuario"}
                    </Typography>
                    <div className={classes.button}>
                        {
                            !user && <Link to="signin">
                                <Button variant="outlined" color="white">
                                    <strong>Iniciar sesion</strong>
                                </Button>
                            </Link>
                        }
                        {
                            user && <Button variant="outlined" color="white" onClick={handleSignOut}>
                                <strong>Cerrar sesion</strong>
                            </Button>
                        }
                        <IconButton aria-label="show cart items" color="inherit" href="/cart">
                            <Badge badgeContent={totalUniqueItems} color="secondary">
                                <ShoppingCartIcon fontSize="large" color="white"/>
                            </Badge>
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>
        </div>
    );
}