import React from 'react';
import { useCart } from 'react-use-cart';
import accounting from 'accounting';

//---------- Material UI ----------//
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  action: {
    marginTop: "1rem",
  },
  media: {
    height: 0,
    paddingTop: '56.25%',
  },
  button: {
    backgroundColor: "#4db6ac",
    color: "white",
  }
}));

const Product = (props) => {
  const { addItem } = useCart();
  const { product } = props;
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardMedia className={classes.media} image={product.image} title={product.name}/>
      <CardContent>
        <Typography variant="h5" component="h2">
          {accounting.formatMoney(product.price, "$")}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {product.name}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <Button onClick={() => addItem(product)} className={classes.button} aria-label="add to Cart">
          Añadir al carrito
          <AddShoppingCartIcon fontSize="large"/>
        </Button>
        <IconButton>
          {Array(3).fill().map((_, i) =>(<p>&#11088;</p>))}
        </IconButton>
      </CardActions>
    </Card>
  );
}

export default Product;