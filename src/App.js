import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { CartProvider } from 'react-use-cart';
import { getProducts } from './api';

//----------Components----------//
import ListProducts from './components/ListProducts';
import Navbar from './components/Navbar';
import Cart from './components/Cart';
import SignIn from './components/Login/SignIn';
import SignUp from './components/Login/SignUp';
import Voucher from './components/Voucher';

function App() {
  const [products, setProducts] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(12);

  const fetchProducts = async () => {
    try {
      const data = await getProducts();
      setProducts(data);
    } catch (error) {
    }
  }

  useEffect(() => {
    fetchProducts();
  }, []);

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = products.slice(indexOfFirstPost, indexOfLastPost)

  return (
    <CartProvider>
      <div className="app">
        <Router>
          <Navbar/>
          <Switch>
            <div>
              <Route exact path="/">
                <ListProducts products={currentPosts} postsPerPage={setCurrentPage}/>
              </Route>
              <Route exact path="/signin" component={SignIn}/>
              <Route exact path="/signup" component={SignUp}/>
              <Route exact path="/cart" component={Cart}/>
              <Route exact path="/voucher" component={Voucher}/>
            </div>
          </Switch>
        </Router>
      </div>
    </CartProvider>
  );
}

export default App;